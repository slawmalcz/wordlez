﻿namespace Wordlez.Data
{
    interface IMultipleChoiceTestService
    {
        public Task<MultipleChoiceTest> GenerateTest(int numberOfPossibleAnswers);
    }
}
