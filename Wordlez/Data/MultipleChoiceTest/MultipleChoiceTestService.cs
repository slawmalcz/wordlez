﻿namespace Wordlez.Data
{
    public class MultipleChoiceTestService : IMultipleChoiceTestService
    {
        private readonly ApplicationDbContext _context;

        public MultipleChoiceTestService(ApplicationDbContext context)
        {
            _context = context;
            
        }

        public Task<MultipleChoiceTest> GenerateTest(int numberOfPossibleAnswers)
        {
            int translationDataCount = _context.TranslationDatas.Count();
            if (numberOfPossibleAnswers == 0 || numberOfPossibleAnswers > translationDataCount)
                throw new Exception("Not possible to create required test");
            var randomSeed = new Random();
            var selectedTranslation = _context.TranslationDatas.ToArray()[randomSeed.Next(translationDataCount)];
            var generateedPossibleAnswer = new List<string>();
            generateedPossibleAnswer.Add(selectedTranslation.TranslatedWord);
            while(generateedPossibleAnswer.Count < numberOfPossibleAnswers)
            {
                var possibleWordToAdd = _context.TranslationDatas.ToArray()[randomSeed.Next(translationDataCount)].TranslatedWord;
                if (!generateedPossibleAnswer.Contains(possibleWordToAdd))
                    generateedPossibleAnswer.Add(possibleWordToAdd);
            }
            return Task.FromResult(new MultipleChoiceTest(selectedTranslation,generateedPossibleAnswer.ToArray()));
        }
    }
}
