﻿namespace Wordlez.Data
{
    public class MultipleChoiceTest
    {
        public MultipleChoiceTest(TranslationData translationData, string[] possibleAnswers)
        {
            TranslationData = translationData;
            PossibleAnswers = possibleAnswers;
        }

        public TranslationData TranslationData { get; set; }
        public string[] PossibleAnswers { get; private set; }


        public bool CheckAnswer(string answer)
        {
            return answer.Equals(TranslationData.TranslatedWord);
        }
    }
}
