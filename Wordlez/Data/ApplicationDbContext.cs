﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Wordlez.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<TranslationData> TranslationDatas { get; set; }
        public DbSet<AccountPreferences> AccountPreferences { get; set; }
        public DbSet<Language> Languages { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
    }
}