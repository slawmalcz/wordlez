﻿namespace Wordlez.Data
{
    public interface IAccountPreferencesService
    {
        AccountPreferences GetAccountPreferences(string userName);
        void AddNewAccountPreferences(AccountPreferences preferences);
        void UpdateAccountPreferences(AccountPreferences preferences);
        void DeleteAccountPreferences(string userName);
    }
}
