﻿using Microsoft.EntityFrameworkCore;
using Wordlez.Pages;

namespace Wordlez.Data
{
    public class AccountPreferencesService : IAccountPreferencesService
    {
        private readonly ApplicationDbContext _context;

        public AccountPreferencesService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddNewAccountPreferences(AccountPreferences preferences)
        {
            try
            {
                _context.AccountPreferences.Add(preferences);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void DeleteAccountPreferences(string userName)
        {
            try
            {
                AccountPreferences test = _context.AccountPreferences.Find(userName);
                _context.AccountPreferences.Remove(test);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public AccountPreferences GetAccountPreferences(string userName)
        {
            try
            {
                return _context.AccountPreferences.First(x => x.UserName == userName);
            }
            catch
            {
                return null;
            }
        }

        public void UpdateAccountPreferences(AccountPreferences preferences)
        {
            try
            {
                var local = _context.Set<AccountPreferences>().Local.FirstOrDefault(entry => entry.UserName.Equals(preferences.UserName));
                // check if local is not null
                if (local != null)
                {
                    // detach
                    _context.Entry(local).State = EntityState.Detached;
                }
                _context.Entry(preferences).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
