﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Wordlez.Data
{
    public class AccountPreferences
    {
        [Key]
        [Required]
        public string UserName { get; set; }

        [Required]
        [ForeignKey("Language")]
        public int OrginalLanguage { get; set; }
        [Required]
        [ForeignKey("Language")]
        public int DesiredLanguage { get; set; }

    }
}
