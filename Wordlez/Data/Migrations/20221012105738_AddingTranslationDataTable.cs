﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Wordlez.Data.Migrations
{
    public partial class AddingTranslationDataTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MultipleChoiceTestDatas");

            migrationBuilder.CreateTable(
                name: "TranslationDatas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrginalWord = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TranslatedWord = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrginalLanguage = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TranslatedLanguage = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TranslationDatas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TranslationDatas");

            migrationBuilder.CreateTable(
                name: "MultipleChoiceTestDatas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PossibleAnswer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TestAnswer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TestQuestions = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MultipleChoiceTestDatas", x => x.Id);
                });
        }
    }
}
