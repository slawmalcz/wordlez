﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Wordlez.Data.Migrations
{
    public partial class AddingMTest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MultipleChoiceTestDatas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TestQuestions = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TestAnswer = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PossibleAnswer = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MultipleChoiceTestDatas", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MultipleChoiceTestDatas");
        }
    }
}
