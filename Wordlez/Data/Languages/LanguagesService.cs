﻿using Microsoft.EntityFrameworkCore;

namespace Wordlez.Data
{
    public class LanguagesService : ILanguagesService
    {
        private readonly ApplicationDbContext _context;

        public LanguagesService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddNewLanguage(Language language)
        {
            try
            {
                _context.Languages.Add(language);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void DeleteLanguage(int Id)
        {
            try
            {
                Language test = _context.Languages.First(x=>x.Id == Id);
                _context.Languages.Remove(test);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Language GetLanguage(int Id)
        {
            try
            {
                Language test = _context.Languages.First(x => x.Id == Id);
                return test;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Language> GetLanguages()
        {
            try
            {
                return _context.Languages;
            }
            catch
            {
                return null;
            }
        }

        public void UpdateLanguage(Language language)
        {
            try
            {
                var local = _context.Set<Language>().Local.FirstOrDefault(entry => entry.Id ==language.Id);
                // check if local is not null
                if (local != null)
                {
                    // detach
                    _context.Entry(local).State = EntityState.Detached;
                }
                _context.Entry(language).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
