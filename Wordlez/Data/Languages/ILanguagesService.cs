﻿namespace Wordlez.Data
{
    public interface ILanguagesService
    {
        IEnumerable<Language> GetLanguages();

        Language GetLanguage(int languageId);
        void AddNewLanguage(Language preferences);
        void UpdateLanguage(Language preferences);
        void DeleteLanguage(int Id);
    }
}
