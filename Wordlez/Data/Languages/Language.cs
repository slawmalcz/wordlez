﻿namespace Wordlez.Data
{
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }

        public string GetDisplay() { return $"({ShortName}){Name}"; }
    }
}
