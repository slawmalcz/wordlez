﻿namespace Wordlez.Data
{
    public class ErrorDisplayService
    {
        public string CallerPage { get; set; }
        public string ErrorMesage { get; set; }
        public string[] ErrorMessageDetails { get; set; }
        public string DestinationPage { get; set; }

        public void CleraData()
        {
            CallerPage = null;
            ErrorMesage = null;
            ErrorMessageDetails = null;
            DestinationPage = null;
        }
    }
}
