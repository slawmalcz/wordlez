﻿namespace Wordlez.Data
{
    interface ITranslationDataService
    {
        public Task<IEnumerable<TranslationData>> GetTranslationDatasAsync();
        IEnumerable<TranslationData> GetTranslationDatas();
        void AddNewTranslationData(TranslationData translationData);
        void UpdateTranslationData(TranslationData translationData);
        public Task<TranslationData> GetTranslationDataAsync(int id);
        TranslationData GetTranslationData(int id);
        void DeleteTranslationData(int id);
    }
}
