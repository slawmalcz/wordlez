﻿using Microsoft.EntityFrameworkCore;

namespace Wordlez.Data
{
    public class TranslationDataService : ITranslationDataService
    {
        private readonly ApplicationDbContext _context;

        public TranslationDataService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void AddNewTranslationData(TranslationData multipleChoiceTest)
        {
            try
            {
                _context.TranslationDatas.Add(multipleChoiceTest);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public void DeleteTranslationData(int id)
        {
            try
            {
                TranslationData test = _context.TranslationDatas.Find(id);
                _context.TranslationDatas.Remove(test);
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }

        public Task<IEnumerable<TranslationData>> GetTranslationDatasAsync()
        {
            return Task.FromResult(GetTranslationDatas());
        }

        public IEnumerable<TranslationData> GetTranslationDatas()
        {
            try
            {
                return _context.TranslationDatas.ToList();
            }
            catch
            {
                throw;
            }
        }

        public Task<TranslationData> GetTranslationDataAsync(int id)
        {
            return Task.FromResult(GetTranslationData(id));
        }

        public TranslationData GetTranslationData(int id)
        {
            try
            {
                return _context.TranslationDatas.First(x => x.Id == id);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateTranslationData(TranslationData multipleChoiceTest)
        {
            try
            {
                var local = _context.Set<TranslationData>().Local.FirstOrDefault(entry => entry.Id.Equals(multipleChoiceTest.Id));
                // check if local is not null
                if (local != null)
                {
                    // detach
                    _context.Entry(local).State = EntityState.Detached;
                }
                _context.Entry(multipleChoiceTest).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch
            {
                throw;
            }
        }
    }
}
