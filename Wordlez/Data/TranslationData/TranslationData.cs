﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Wordlez.Data
{
    public class TranslationData
    {
        public int Id { get; set; }
        [Required]
        public string OrginalWord { get; set; }
        [Required]
        public string TranslatedWord { get; set; }

        [Required]
        [ForeignKey("Language")]
        public int OrginalLanguage { get; set; }
        [Required]
        [ForeignKey("Language")]
        public int TranslatedLanguage { get; set; }
    }
}
